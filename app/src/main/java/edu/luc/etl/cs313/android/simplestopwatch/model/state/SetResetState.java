package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

public class SetResetState implements StopwatchState{

    public SetResetState(final StopwatchSMStateView sm){ this.sm = sm; }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStop();
        sm.toStoppedState();

    }


    @Override
    public void onTick(){sm.actionAlarm();}

    public void updateView() {
        sm.updateUIRuntime();
    }

    public int getId()
    {
        return R.string.SET_RESET;
    }

}
