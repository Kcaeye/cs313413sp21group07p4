package edu.luc.etl.cs313.android.simplestopwatch.model.time;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.*;

/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int runningTime = 0;

    protected final int min = 0;

    protected final int max = 99;

    //private int lapTime = -1;

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        if(!full()){
            runningTime = runningTime +1;
       }
    }

    @Override
    public void decRuntime() {
        if(!vacant()){
            runningTime = runningTime -1;
        }
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public boolean full() {
        return runningTime >= max;
    }

    @Override
    public boolean vacant(){
        return runningTime <= min;
    }

    //@Override
    //public void setLaptime() {
        //lapTime = runningTime;
    }

    //@Override
    //public int getLaptime() {
        //return lapTime;
    //}
//}