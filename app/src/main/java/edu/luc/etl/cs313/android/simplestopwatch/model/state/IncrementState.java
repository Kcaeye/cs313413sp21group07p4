package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class IncrementState implements StopwatchState {

    public IncrementState(final StopwatchSMStateView sm) { this.sm = sm; }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionInc();
        sm.toIncrementState();

    }

    @Override
    public void onTick() {
        int delay = sm.getTimeElapsed();
        sm.setTimeElapsed(++delay);
        if(delay == 3 || sm.timeIsUp()){
            sm.actionAlarm();
            sm.toRunningState();
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();

    }

    @Override
    public int getId(){
        return R.string.INCREMENT;
    }
}
